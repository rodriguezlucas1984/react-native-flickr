import React, {useState, useEffect} from 'react';
import {FlatList, Text, View} from 'react-native';
import axios from 'axios';
import PhotoDetail from './PhotoDetail';

const PhotoList = ({navigation, route}) => {
  const [state, setstate] = useState({loading: true, error: null, data: []});

  useEffect(() => {
    axios
      .get(
        `https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=6e8a597cb502b7b95dbd46a46e25db8d&photoset_id=${route.params.albumId}&user_id=137290658%40N08&format=json&nojsoncallback=1`,
      )
      .then((response) => {
        setstate({
          loading: false,
          error: null,
          data: response.data.photoset.photo,
        });
      })
      .catch((err) => {
        setstate({loading: false, error: err, data: []});
      });
  }, [true]);

  const renderPhoto = ({item}) => {
    return (
      <PhotoDetail
        key={item.title}
        navigation={navigation}
        photoId={item.id}
        title={item.title}
        imageUrl={`https://farm${item.farm}.staticflickr.com/${item.server}/${item.id}_${item.secret}.jpg`}
      />
    );
  };

  if (state.loading) {
    return (
      <View style={{flex: 1}}>
        <Text>Loading...</Text>
      </View>
    );
  }
  if (state.error) {
    return (
      <View style={{flex: 1}}>
        <Text>{state.error.toString()}</Text>
      </View>
    );
  }
  if (state.data) {
    return (
      <View style={{flex: 1}}>
        <FlatList
          data={state.data}
          renderItem={renderPhoto}
          keyExtractor={(item, index) => {
            return item.id;
          }}
        />
      </View>
    );
  }
};

export default PhotoList;
