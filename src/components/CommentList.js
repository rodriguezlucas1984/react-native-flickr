import React, {useEffect, useState} from 'react';
import {Text, View, FlatList} from 'react-native';
import axios from 'axios';

const CommentList = ({photoId}) => {
  const [state, setstate] = useState({loading: true, error: null, data: []});

  useEffect(() => {
    axios
      .get(
        `https://api.flickr.com/services/rest/?method=flickr.photos.comments.getList&api_key=6e8a597cb502b7b95dbd46a46e25db8d&photo_id=${photoId}&format=json&nojsoncallback=1`,
      )
      .then((response) => {
        setstate({
          loading: false,
          error: null,
          data: response.data.comments.comment,
        });
      })
      .catch((err) => {
        setstate({loading: false, error: err, data: []});
      });
  }, [true]);

  // console.log(state);
  const renderComment = ({item}) => {
    return (
      <View style={{margin: '3%'}}>
        <Text style={[{fontWeight: 'bold'}, {fontSize: 18}]}>
          {item.authorname.toString()}:
        </Text>
        <Text style={[{paddingLeft: '5%'}, {fontSize: 15}]}>
          {item._content.toString()}
        </Text>
      </View>
    );
  };

  if (state.loading) {
    return <Text>Loading.....</Text>;
  }

  if (state.error) {
    return <Text>{state.error.toString()}</Text>;
  }

  if (state.data && state.data.length > 0) {
    return (
      <View>
        <FlatList
          data={state.data}
          renderItem={renderComment}
          keyExtractor={(item, index) => {
            return item.id;
          }}
        />
      </View>
    );
  }

  if (state.data && state.data.length === 0) {
    return (
      <View>
        <Text style={[{fontWeight: 'bold'}, {fontSize: 23}]}>
          No hay comentarios....
        </Text>
      </View>
    );
  }
};

export default CommentList;
