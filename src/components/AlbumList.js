import React, {useEffect, useState} from 'react';
import {Text, View, FlatList} from 'react-native';
import axios from 'axios';
import AlbumDetail from './AlbumDetail';

const AlbumList = ({navigation}) => {
  const [state, setstate] = useState({loading: true, error: null, data: []});

  useEffect(() => {
    axios
      .get(
        'https://api.flickr.com/services/rest/?method=flickr.photosets.getList&api_key=6e8a597cb502b7b95dbd46a46e25db8d&user_id=137290658%40N08&format=json&nojsoncallback=1',
        {timeout: 5000},
      )
      .then((response) =>
        setstate({
          loading: false,
          error: null,
          data: response.data.photosets.photoset,
        }),
      )
      .catch((err) => {
        setstate({loading: false, error: err, data: []});
      });
  }, [true]);

  const renderAlbum = ({item}) => {
    return (
      <AlbumDetail
        key={item.id}
        navigation={navigation}
        title={item.title._content}
        albumId={item.id}
      />
    );
  };
  // console.log(state.data);

  if (state.loading) {
    return <Text>Loading...</Text>;
  }
  if (state.error) {
    return <Text>{state.error.toString()}</Text>;
  }

  if (state.data) {
    return (
      <View style={{flex: 1}}>
        <FlatList
          data={state.data}
          renderItem={renderAlbum}
          keyExtractor={(item, index) => {
            return item.id;
          }}
        />
      </View>
    );
  }
};

export default AlbumList;
